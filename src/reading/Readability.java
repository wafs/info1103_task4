package reading;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Estimate the readability of text files. The formula for Flech Reading Ease
 * (FRE) is given by:
 * 
 * <br />
 * <br />
 * FRE = 206.835 - (1.015 x ASL) - (84.6 x ASW) <br />
 * <br />
 * where FRE = Flech Reading Ease, ASL = Average Sentence Length, ASW = Average
 * Syllables per Word. Reference: readabilityformulas.com
 */

public class Readability {

	/**
	 * Calculates the FRE.
	 * 
	 * @param nWords
	 *            total count of words
	 * @param nSyllables
	 *            total count of syllables
	 * @param nSentences
	 *            total count of sentences
	 * @return Score of FRE. If any params are invalid, returns -1.
	 */
	public static double getFRE(int nWords, int nSyllables, int nSentences) {
		if (nWords < 1 || nSyllables < 1 || nSentences < 1) {
			return -1;
		} else {
			double asl = (double) nWords / nSentences;
			double asw = (double) nSyllables / nWords;

			return (206.835 - (1.015 * asl) - (84.6 * asw));
		}
	};

	/**
	 * Checks if char is a vowel by testing if the character is in the
	 * AEIOUaeiou index.
	 * 
	 * @param word
	 * @return boolean : True if vowel, False if not vowel.
	 */
	// due to 
	public static boolean isVowel(char character) {
		return "AEIOUYaeiouy".indexOf(character) != -1;
	}

	/**
	 * Searches for a group of 1 or more vowels. Each one of those groups counts
	 * as a syllable.
	 * 
	 * @return integer of amount of syllables found, if none are found, it
	 *         returns a 1.
	 */
	public static int getNumSyllables(String word) {
		int syllableCount = 0;
		if(word.isEmpty() || word.equals("\n")){
			//syllableCount = 1;
			return syllableCount;
		}
		// regex pattern for "one or more of the letters between the brackets"
		Pattern pattern = Pattern.compile("[aeiouyAEIOUY]+");
		Matcher matcher = pattern.matcher(word);


		while (matcher.find()) {
			syllableCount++;
		}

		if (syllableCount == 0) {
			syllableCount = 1;
		}
		return syllableCount;
	}

	
	public static int getNumSentences(String sentence) {
		int sentenceCount = 0;
		if(sentence.isEmpty()){
			return sentenceCount;
		}
		// regex pattern for "one or more of the letters between the brackets"
		Pattern pattern = Pattern.compile("[.!?]+");
		Matcher matcher = pattern.matcher(sentence);


		while (matcher.find()) {
			sentenceCount++;
		}

		return sentenceCount;
	}
	
	/**
	 * Counts the length of the sentence by splitting on whitespace and adding words into an array, 
	 * and then counts the array length.
	 * @param text String input of the sentence
	 * @return Integer of sentence length
	 */
	public static int countSentenceLength(String text) {
		// regex pattern for "one or more whitespace characters"
		String tmpArray[] = text.split("(\\s)+");
		int sentenceSize = 0;

		for(int i = 0; i < tmpArray.length; i++){
			if(!tmpArray[i].isEmpty()){
					sentenceSize++;
			}
		}
		return sentenceSize;
	}

	/**
	 * Reads text file and converts it into a string.
	 * @param fileName file name given from args[0] in the main method.
	 * @return String containing the file's contents.
	 */
	public static String readFile(String fileName) {
		BufferedReader textReader = null;
		String tmpDoc = "";
		try {
			FileReader fr = new FileReader(fileName);
			textReader = new BufferedReader(fr);

			String line;
			
			while ((line = textReader.readLine()) != null) {
				tmpDoc += line + "\n";
			}

		} catch (IOException e) {
		}

		finally {
			try {
				textReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return tmpDoc;

	}

	/**
	 * Takes in a file (provided by filename from command arguments) and generates 
	 * a FRE from it. If the file is not found the program exits. 
	 * @param args
	 */
	public static void main(String[] args) {
		 int totalSentences = 0;
		 int totalSyllables = 0;
		 int totalWords = 0;
		 String document = "";
		

		// exit if invalid amount of args.
		if (args.length != 1) {
			return;
		}

		// create file and variable out of arg.
		try {
			String file = args[0];
			document = readFile(file);
		} catch (Exception e) {
			System.out.println("I cannot open file " + args[0]);
			return;
		}

		ArrayList<String> aSentences = new ArrayList<String>();

		// split up document into sentences using regex pattern of
		// "one or more from .?!"

		
		// convert all forms of delimeters(singular or groups) into a period. 
		document = document.replaceAll("[.!?]+", ".");
		// split document into sentences at (new) delimeters,
		// regex lookahead to make sure java's split method doesn't remove the delimeter. 
		for (String sentence : document.split("(?<=.)")) {
			if (sentence.equals("\n")){
				continue;
			}
			aSentences.add(sentence.trim());
		}
		
		// Add each "word" (defined as any character surrounded by whitespace)
		// to wordcount.
		for (String words : document.split("\\s+")) {
			// add each word to the wordcount
			totalWords++;
			// calculate syllables here
			totalSyllables += getNumSyllables(words);
		}

		
		totalSentences = getNumSentences(document);
			
		//System.out.printf("Sentences:%d , Syllables:%d,  Words:%d\n" ,totalSentences, totalSyllables, totalWords);
		System.out.println("Readability of file " + args[0] + " = "	+ getFRE(totalWords, totalSyllables, totalSentences));
	}
}

